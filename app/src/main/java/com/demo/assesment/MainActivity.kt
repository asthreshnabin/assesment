package com.demo.assesment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.ViewGroup
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.demo.assesment.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val imageRequestCode = 1
    private val selectedImages = mutableListOf<String>()
    private var binding: ActivityMainBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        binding?.btnSelectImage?.setOnClickListener { selectImage() }
        binding?.btnGenerateList?.setOnClickListener { generateImageViewList() }
    }

    private fun selectImage() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.type = "image/*"
        startActivityForResult(intent, imageRequestCode)
    }

    private fun generateImageViewList() {
        binding?.gdvImage?.removeAllViewsInLayout()
        val imageListCount = binding?.edtListSize?.text.toString().toIntOrNull()
        if (imageListCount != null && imageListCount > 0) {
            if (selectedImages.size == 2) {
                val dogImage = selectedImages[0]
                val catImage = selectedImages[1]

                val dogIndexList = mutableListOf<Int>()
                var sum = 0
                var i = 1
                while (sum < imageListCount) {
                    sum += i
                    dogIndexList.add(sum)
                    i++
                }

                binding?.gdvImage?.alignmentMode = GridLayout.ALIGN_BOUNDS
                binding?.gdvImage?.columnCount = 3
                for (j in 1..imageListCount) {
                    val imageView = ImageView(this)
                    val layoutParams = ViewGroup.LayoutParams(
                        220,
                        220
                    )
                    imageView.layoutParams = layoutParams
                    imageView.setPadding(8, 8, 8, 8)

                    if (j in dogIndexList) {
                        Glide.with(this)
                            .load(dogImage.toUri())
                            .into(imageView)
                    } else {
                        Glide.with(this)
                            .load(catImage.toUri())
                            .into(imageView)
                    }
                    binding?.gdvImage?.addView(imageView)
                }
            } else {
                showToast("Please select exactly 2 images.")
            }
        } else {
            showToast("Please enter a valid list size.")
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == imageRequestCode && resultCode == Activity.RESULT_OK && data != null) {
            if (data.clipData != null) {
                val count = data.clipData!!.itemCount
                selectedImages.clear()
                for (i in 0 until count) {
                    val imageUri = data.clipData!!.getItemAt(i).uri.toString()
                    selectedImages.add(imageUri)
                }
            } else if (data.data != null) {
                val imageUri = data.data.toString()
                selectedImages.clear()
                selectedImages.add(imageUri)
            }
        }
    }


}
